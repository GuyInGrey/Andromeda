﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace Andromeda.Modules
{
    [Name("Config")]
    [Group("c")]
    [Summary("Commands related to configuring server-based options.")]
    public class Config : AndromedaCommand
    {
        [Command("reset")]
        [Name("Reset Config")]
        [Summary("Resets the server-wide configuration to defaults. If no config exists, creates a new one.")]
        [RequireUserPermission(GuildPermission.ManageChannels)]
        public Task Reset()
        {
            Configuration.ResetServer(Context.Guild.Id);

            AtReply(" - Config - ", "Config Reset", $"The config was reset. New prefix is:\n`{Configuration.GetValue(Context.Guild.Id, "prefix")}`");

            return Task.CompletedTask;
        }

        [Command("set")]
        [Name("Set Value")]
        [Summary("Sets the given value to the given key.")]
        [RequireUserPermission(GuildPermission.ManageChannels)]
        public Task Set(string key, string value)
        {
            if (Configuration.ContainsServerConfig(Context.Guild.Id))
            {
                Configuration.SetValue(Context.Guild.Id, key, value);
            }
            else
            {
                AtReplyError(" - Config - ", "Value Set", $"Failed to set value: This sever does not contain a config on the bot. Run the following command to reset the config.\n`{Configuration.GetPrefix(Context.Guild.Id)}c reset`");
                return Task.CompletedTask;
            }

            AtReply(" - Config - ", "Value Set", $"Correctly set value.\n\nKey: `{key}`\n\nValue: `{value}`");

            return Task.CompletedTask;
        }

        [Command("get")]
        [Name("Get Value")]
        [Summary("Get the given value from the given key.")]
        [RequireUserPermission(GuildPermission.ManageChannels)]
        public Task Get(string key)
        {
            if (!Configuration.ContainsServerConfig(Context.Guild.Id))
            {
                AtReplyError(" - Config - ", "Value Set", $"Failed to get value: This sever does not contain a config on the bot. Run the following command to reset the config.\n`{Configuration.GetPrefix(Context.Guild.Id)}c reset`");
                return Task.CompletedTask;
            }

            var value = Configuration.GetValue(Context.Guild.Id, key);
            AtReply(" - Config - ", "Value Get", $"Key: `{key}`\n\nValue: `{value ?? "null"}`");

            return Task.CompletedTask;
        }

        [Command("list")]
        [Name("List Configurations")]
        [Summary("Lists all the config options set on this server.")]
        [RequireUserPermission(GuildPermission.ManageChannels)]
        public Task List()
        {
            if (!Configuration.ContainsServerConfig(Context.Guild.Id))
            {
                AtReplyError(" - Config - ", "Value Set", $"Failed to list values: This sever does not contain a config on the bot. Run the following command to reset the config.\n`{Configuration.GetPrefix(Context.Guild.Id)}c reset`");
                return Task.CompletedTask;
            }

            var values = Configuration.ListOptions(Context.Guild.Id);
            var outString = "";

            foreach (var value in values)
            {
                outString += $"{value.Key}: `{value.Value.ToString()}`\n";
            }

            AtReply(" - COnfig - ", $"Value List For Server {Context.Guild.Name}", outString);

            return Task.CompletedTask;
        }
    }
}