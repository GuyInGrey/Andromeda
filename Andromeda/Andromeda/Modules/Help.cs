﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace Andromeda.Modules
{
    [Summary("Commands that will provide assistance in usage of the bot.")]
    public class Help : AndromedaCommand
    {
        private readonly CommandService _service;

        public Help(CommandService service)
        {
            _service = service;
        }

        [Command("help")]
        [Name("Help")]
        [Summary("Shows the help menu.")]
        [Alias("commands", "halp")]
        public async Task HelpAsync([Remainder] string module = null)
        {
            var replyMessage = "";
            var prefix = Configuration.GetPrefix(Context.Guild.Id);

            if (module == null)
            {
                foreach (var serviceModules in _service.Modules)
                {
                    if (serviceModules.Name.ToLower() != "andromedacommand")
                    {
                        replyMessage += $"\n Module `{serviceModules.Name}`: {serviceModules.Summary}";
                        replyMessage += $"\n  -  {prefix}help {serviceModules.Name}\n";
                    }
                }
            }
            else
            {
                foreach (var servModule in _service.Modules)
                {
                    if (servModule.Aliases.FirstOrDefault(x => x.ToLower().Replace(" ", "") == module.ToLower().Replace(" ", "")) != null
                        || servModule.Name.ToLower() == module.ToLower().Replace(" ", ""))
                    {
                        if (servModule.Name.ToLower() != "andromedacommand")
                        {
                            var aliasString = $"{{";

                            foreach (var al in servModule.Aliases)
                            {
                                aliasString += $"{al} | ";
                            }

                            aliasString = aliasString.Substring(0, aliasString.Length - 3) + "}";
                            aliasString = aliasString == "{}" ? "" : aliasString;

                            replyMessage += $" - `{servModule.Name}` - {aliasString} \n\n{servModule.Summary} \n\n";

                            foreach (var cmd in servModule.Commands)
                            {
                                if (cmd.Name != "HIDDEN")
                                {
                                    var result = await cmd.CheckPreconditionsAsync(Context);
                                    if (result.IsSuccess)
                                    {
                                        var aliasReturn = "{";
                                        foreach (var alias in cmd.Aliases)
                                        {
                                            aliasReturn += alias + " | ";
                                        }

                                        aliasReturn = aliasReturn.Substring(0, aliasReturn.Length - 3);
                                        aliasReturn += "}";

                                        var parameterReturn = "";
                                        if (cmd.Parameters.Count > 0)
                                        {
                                            parameterReturn = "( ";
                                            foreach (var parameter in cmd.Parameters)
                                            {
                                                parameterReturn += parameter.IsOptional ? " __***O***__ " : "";
                                                parameterReturn += parameter.IsMultiple ? " __***M***__ " : "";
                                                parameterReturn += parameter.IsRemainder ? " __***R***__ " : "";
                                                parameterReturn += parameter.Name;
                                                parameterReturn += ", ";
                                            }

                                            parameterReturn = parameterReturn.Substring(0, parameterReturn.Length - 2);
                                            parameterReturn += " )";
                                        }

                                        replyMessage += $"- __{cmd.Name}__\n  - Summary: {cmd.Summary}\n    - Usage: {prefix}{aliasReturn}{parameterReturn}\n";
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (replyMessage != "")
            {
                AtReply("   -- Help Menu --   ",
                    "These are valid commands to use with the Atled Bot!",
                    replyMessage.Substring(1, replyMessage.Length - 1));
            }
            else
            {
                AtReply("   -- Help Menu --   ",
                    "Failure",
                    $"The module, `{module}`, is not a valid module. Use `$help` for valid modules.");
            }
        }

        [Command("easteregg")]
        [Name("HIDDEN")]
        public async Task EasterEggAsync()
        {
            await Context.Message.DeleteAsync();
            await (Context.Message.Author as IUser).SendMessageAsync("Meaning of life: 42");
        }
    }
}