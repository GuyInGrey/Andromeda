﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Andromeda.Games;
using Discord.Commands;
using Discord.WebSocket;

namespace Andromeda.Modules
{
    [Name("Game")]
    [Group("g")]
    [Summary("Commands related to games and fun!")]
    public class Game : AndromedaCommand
    {
        [Command("start")]
        [Name("Start Game")]
        [Summary("Starts the game specified for the party members specified.")]
        public Task Start(string gameName, params SocketGuildUser[] players)
        {
            var party = Context.Message.Author.Party();

            if (party == null)
            {
                AtReplyError(" - Game - ", "Game Error", "You are not the leader of any party.");
                return Task.CompletedTask;
            }

            if (party.Leader.Id != Context.Message.Author.Id)
            {
                AtReplyError(" - Game - ", "Game Error", "You are not the leader of any party.");
                return Task.CompletedTask;
            }

            var playersNotInParty = new List<SocketGuildUser>();

            foreach (var p in players)
            {
                if (party.Players.FirstOrDefault(x => x.Id == p.Id) == null)
                {
                    playersNotInParty.Add(p);
                }
            }

            if (playersNotInParty.Count > 0)
            {
                var responseString = "";

                foreach (var p in playersNotInParty)
                {
                    responseString += $"{p.Nickname} ({p.Username}#{p.Discriminator})\n";
                }

                AtReplyError(" - Game - ", "The following players which you are attemping to play a game with are not in the party: ", responseString);
                return Task.CompletedTask;
            }
            
            switch (gameName)
            {
                case "tictactoe":
                    if (players.Length != 2)
                    {
                        AtReplyError(" - Game - ", "Game Error", "Invalid Player Count. Requires 2.");
                        return Task.CompletedTask;
                    }
                    party.Game = new TicTacToe(party.GUID, players);
                    break;
                default:
                    AtReplyError(" - Game - ", "Game Error", "The specified game does not exist.");
                    break;
            }

            return Task.CompletedTask;
        }
    }
}