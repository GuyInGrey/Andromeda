﻿using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;

namespace Andromeda.Modules
{
    [Name("Party")]
    [Group("p")]
    [Summary("Commands related to parties, invites, and grouping.")]
    public class Party : AndromedaCommand
    {
        [Command("invite")]
        [Name("Invite To Party")]
        [Summary("Invites a specific person to a party group.")]
        public Task Invite(SocketGuildUser user)
        {
            Andromeda.Party userParty = null;
            var prefix = Configuration.GetPrefix(Context.Guild.Id);

            foreach (var party in Andromeda.Party.Parties)
            {
                if (party.Leader?.Id == Context.Message.Author.Id)
                {
                    userParty = party;
                }
            }

            if (userParty == null)
            {
                AtReplyError(" - Party - ", "Party Error", "You are not currently the leader of any party. You must be the leader of a party to invite.");
                return Task.CompletedTask;
            }

            var userIsInParty = Andromeda.Party.FindParty(user) != null;

            if (userIsInParty)
            {
                AtReplyError(" - Party - ", "Party Error", "The user is already in a party.");
                return Task.CompletedTask;
            }

            foreach (var inv in Andromeda.Invite.Invites)
            {
                if (inv.Invitee.Id == user.Id && inv.InvitingParty.GUID == userParty.GUID)
                {
                    AtReplyError(" - Party - ", "Party Error", "The user already has an invite to the party.");
                    return Task.CompletedTask;
                }
            }

            var invite = new Invite(userParty, user);

            AtReply(" - Party - ", "Party Invite", $"{user.Mention}, you have been invited to a party! Join the party by typing\n`{prefix}p accept {invite.ID}`\nOr decline by typing\n`{prefix}p decline {invite.ID}`\n\nYou can also see the all the invites towards you by typing\n`{prefix}p listinvites`");

            return Task.CompletedTask;
        }

        [Command("accept")]
        [Name("Accept Invite")]
        [Summary("Accepts an invite to a party.")]
        public Task AcceptInvite(int inviteID)
        {
            var prefix = Configuration.GetPrefix(Context.Guild.Id);

            if (inviteID >= Andromeda.Invite.Invites.Count || inviteID < 0 || Andromeda.Invite.Invites[inviteID].Invitee.Id != Context.Message.Author.Id)
            {
                AtReplyError(" - Party - ", "Party Invite", $"Invalid Invite ID. To see valid invites, type\n`{prefix}p listinvites`");
                return Task.CompletedTask;
            }
            
            if (Andromeda.Party.FindParty(Context.Message.Author) != null)
            {
                AtReplyError(" - Party - ", "Party Invite", "You are already in a party.");
                return Task.CompletedTask;
            }

            Andromeda.Invite.Invites[inviteID].Accept();
            AtReplyError(" - Party - ", "Party Invite", "You have accepted the invite.");

            return Task.CompletedTask;
        }

        [Command("decline")]
        [Name("Decline Invite")]
        [Summary("Declines an invite to a party.")]
        public Task DeclineInvite(int inviteID)
        {
            var prefix = Configuration.GetPrefix(Context.Guild.Id);

            if (inviteID > Andromeda.Invite.Invites.Count || inviteID < 0 || Andromeda.Invite.Invites[inviteID].Invitee.Id != Context.Message.Author.Id)
            {
                AtReplyError(" - Party - ", "Party Invite", $"Invalid Invite ID. To see valid invites, type\n`${prefix}p listinvites`");
                return Task.CompletedTask;
            }

            Andromeda.Invite.Invites[inviteID].Cancel();
            AtReplyError(" - Party - ", "Party Invite", "You have declined the invite.");

            return Task.CompletedTask;
        }

        [Command("listinvites")]
        [Name("List Invites")]
        [Summary("Lists all the invites currently available to you.")]
        public Task Inivtes()
        {
            var response = "";
                
            foreach (var invite in Andromeda.Invite.Invites)
            {
                if (invite.Invitee.Id == Context.Message.Author.Id)
                {
                    response += $" - {invite.ID} : {invite.InvitingParty.Leader.Nickname} ({invite.InvitingParty.Leader.Username}${invite.InvitingParty.Leader.Discriminator})";
                }
            }

            response = response == "" ? "There are no invites towards you." : response;

            AtReply(" - Party - ", "List of invites towards you:", response);

            return Task.CompletedTask;
        }

        [Command("create")]
        [Name("Create New Party")]
        [Summary("Creates a private party.")]
        public async Task Create()
        {
            var isInParty = Andromeda.Party.FindParty(Context.Message.Author) != null;

            if (isInParty)
            {
                AtReplyError(" - Party - ", "Party Error", "You are currently in a party. You can only create a party when not in one.");
                return;
            }

            var gameCategoryID = Configuration.GetValue(Context.Guild.Id, "gameCategoryID");

            if (gameCategoryID == null)
            {
                AtReplyError(" - Party - ", "Party Error", "The config `gameCategoryID` does not exist. Please set it using the config commands.");
                return;
            }

            var newParty = new Andromeda.Party(Context.Message.Author as SocketGuildUser);
            await newParty.CreateChannels(newParty.Leader);
            AtReply(" - Party - ", "Party Creation", "Your party has been created.\n\n" + newParty.Channel.Mention);

            return;
        }

        [Command("list")]
        [Name("List Members")]
        [Summary("Lists all the members currently in the party you're in.")]
        public Task List()
        {
            var party = Andromeda.Party.FindParty(Context.Message.Author);

            if (party == null)
            {
                AtReplyError(" - Party - ", "Party Error", "You are currently not in a party.");
                return Task.CompletedTask;
            }

            var response = "";

            foreach (var member in party.Players)
            {
                response += $" - {member.Nickname} ({member.Username}#{member.Discriminator})\n\n";
            }

            AtReply(" - Party - ", "Party Members: ", response);

            return Task.CompletedTask;
        }

        [Command("leave")]
        [Name("Leave Party")]
        [Summary("Leaves the party you are a part of.")]
        public Task Leave()
        {
            var party = Andromeda.Party.FindParty(Context.Message.Author);

            if (party == null)
            {
                AtReplyError(" - Party - ", "Party Error", "You are currently not in a party.");
                return Task.CompletedTask;
            }

            if (party.Leader.Id == Context.Message.Author.Id)
            {
                AtReplyError(" - Party - ", "Party Error", "You are currently the leader of the party. Transfer ownership to another member of the party to leave the party.");
                return Task.CompletedTask;
            }

            party.Kick(Context.Message.Author as SocketGuildUser);
            AtReply(" - Party - ", "Party Leave", "You have left the party.");

            return Task.CompletedTask;
        }

        [Command("transfer")]
        [Name("Transfer Ownership of Party")]
        [Summary("Transfers ownership of the party to the specified person.")]
        public Task Transfer(SocketGuildUser user)
        {
            Andromeda.Party userParty = null;

            foreach (var party in Andromeda.Party.Parties)
            {
                if (party.Leader.Id == Context.Message.Author.Id)
                {
                    userParty = party;
                }
            }

            if (userParty == null)
            {
                AtReplyError(" - Party - ", "Party Error", "You are not currently the leader of any party.");
                return Task.CompletedTask;
            }

            if (!userParty.Players.Contains(user))
            {
                AtReplyError(" - Party - ", "Party Error", "The specified person is not a member of the party.");
                return Task.CompletedTask;
            }

            userParty.Leader = user;

            AtReply(" - Party - ", "Party Transfer", $"{userParty.Leader.Mention}, you are now the party leader! (Given by {Context.Message.Author.Mention})");

            return Task.CompletedTask;
        }

        [Command("delete")]
        [Name("Delete Party")]
        [Summary("Deletes the party you are the leader of.")]
        public async Task Delete()
        {
            Andromeda.Party userParty = null;

            foreach (var party in Andromeda.Party.Parties)
            {
                if (party.Leader.Id == Context.Message.Author.Id)
                {
                    userParty = party;
                }
            }

            if (userParty == null)
            {
                AtReplyError(" - Party - ", "Party Error", "You are not currently the leader of any party.");
                return;
            }

            await userParty.Delete();
            AtReply(" - Party - ", "Party Deletion", "Your party has been deleted.");

            return;
        }

        [Command("kick")]
        [Name("Kick Member")]
        [Summary("Kicks the specified member from the party.")]
        public Task Kick(SocketGuildUser user)
        {
            Andromeda.Party userParty = null;

            foreach (var party in Andromeda.Party.Parties)
            {
                if (party.Leader.Id == Context.Message.Author.Id)
                {
                    userParty = party;
                }
            }

            if (userParty == null)
            {
                AtReplyError(" - Party - ", "Party Error", "You are not currently the leader of any party.");
                return Task.CompletedTask;
            }

            if (user.Id == Context.Message.Author.Id)
            {
                AtReplyError(" - Party - ", "Party Error", "You cannot kick yourself!.");
                return Task.CompletedTask;
            }

            if (!userParty.Players.Contains(user))
            {
                AtReplyError(" - Party - ", "Party Error", "The specified person is not a member of the party.");
                return Task.CompletedTask;
            }

            userParty.Kick(user);

            AtReply(" - Party - ", "Kick", $"{user.Nickname} ({user.Username}#{user.Discriminator}) has been kicked from the party.");

            return Task.CompletedTask;
        }
    }
}   