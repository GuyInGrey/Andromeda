﻿using System;

namespace Andromeda.Logging
{
    public class MessageLoggedEventArgs : EventArgs
    {
        /// <summary>
        /// The message that was given to the logger.
        /// </summary>
        public string InputMessage { get; }

        /// <summary>
        /// The message that was logged.
        /// </summary>
        public string OutputMessage { get; }

        /// <summary>
        /// The Category of the log message.
        /// </summary>
        public LogMessageCategory Category { get; }

        /// <summary>
        /// The time the message was logged.
        /// </summary>
        public DateTime TimeLogged { get; }

        public MessageLoggedEventArgs(string input, LogMessageCategory type, string outputString, DateTime timeLogged)
        {
            InputMessage = "";
            Category = type;
            OutputMessage = outputString;
            TimeLogged = timeLogged;
        }
    }
}
