﻿using System;
using Discord;

namespace Andromeda.Logging
{
    public class LogMessageCategory
    {
        /// <summary>
        /// The color that displays when the message is logged.
        /// </summary>
        public ConsoleColor ConsoleColor { get; set; }

        /// <summary>
        /// The string format of the log type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// The Discord alternative color for the category.
        /// </summary>
        public Color DiscordColor { get; set; }

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="type">The string format of the log type.</param>
        /// <param name="color">The color that displays when the message is logged.</param>
        public LogMessageCategory(string type, ConsoleColor color, Color discordColor)
        {
            ConsoleColor = color;
            Type = type;
            DiscordColor = discordColor;
        }

        public static LogMessageCategory Okay => new LogMessageCategory("Okay", ConsoleColor.Green, Color.Green);
        public static LogMessageCategory Info => new LogMessageCategory("Info", ConsoleColor.White, Color.LightGrey);
        public static LogMessageCategory Warning => new LogMessageCategory("Warning", ConsoleColor.Yellow, Color.LightOrange);
        public static LogMessageCategory Error => new LogMessageCategory("Error", ConsoleColor.Red, Color.Red);
        public static LogMessageCategory Fatal => new LogMessageCategory("Fatal", ConsoleColor.DarkRed, Color.DarkRed);

        /// <summary>
        /// Converts a <see cref="LogSeverity"/> to a <see cref="LogMessageCategory"/>.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static LogMessageCategory Convert(LogSeverity s)
        {
            switch (s)
            {
                case LogSeverity.Critical:
                    return Fatal;
                case LogSeverity.Debug:
                    return Info;
                case LogSeverity.Error:
                    return Error;
                case LogSeverity.Info:
                    return Info;
                case LogSeverity.Verbose:
                    return Warning;
                case LogSeverity.Warning:
                    return Warning;
                default:
                    return Fatal;
            }
        }
    }
}
