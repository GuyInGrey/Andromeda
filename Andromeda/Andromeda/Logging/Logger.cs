﻿using System;

namespace Andromeda.Logging
{
    public static class Logger
    {
        /// <summary>
        /// Occurs whenever a message is logged.
        /// </summary>
        public static event EventHandler<MessageLoggedEventArgs> MessageLogged;

        /// <summary>
        /// Log a message into the console.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="type"></param>
        public static void Log(string message, LogMessageCategory type)
        {
            var time = DateTime.Now;

            Console.ForegroundColor = type.ConsoleColor;

            var outputString = $"[{time}] {type.Type}: {message}";

            Console.WriteLine(outputString);

            Console.ForegroundColor = ConsoleColor.Black;

            MessageLogged?.Invoke(null, new MessageLoggedEventArgs(message, type, outputString, time)); 
        }
    }
}