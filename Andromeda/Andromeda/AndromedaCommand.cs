﻿using System;
using Andromeda.Logging;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace Andromeda
{
    public class AndromedaCommand : ModuleBase<SocketCommandContext>
    {
        public void AtReply(string title, string description, string message)
        {
            ReplyAsync("", false, CreateEmbedResponse(title, description, message, (Color)Program.Config["responseNormal"], GetAuthorOfMessage(Context.Message as SocketMessage)));
        }

        public void AtReplyError(string title, string description, string message)
        {
            ReplyAsync("", false, CreateEmbedResponse(title, description, message, (Color)Program.Config["responseError"], GetAuthorOfMessage(Context.Message as SocketMessage)));
        }

        public IUserMessage AtReplyColor(string title, string description, string message, Color color)
        {
            return ReplyAsync("", false, CreateEmbedResponse(title, description, message, color, GetAuthorOfMessage(Context.Message as SocketMessage))).Result;
        }

        public static Embed CreateEmbedResponse(string title, string description, string text, Color color, EmbedAuthorBuilder author)
        {
            return new EmbedBuilder()
            {
                Color = color,
                Title = $"**{title}**",
                Description = description != "" ? $"*{description.Trim()}*\n\n{text}" : $"{text}",
                Author = author,
            }.Build();
        }

        public static EmbedAuthorBuilder GetAuthorOfMessage(SocketMessage message)
        {
            return new EmbedAuthorBuilder()
            {
                IconUrl = message.Author.GetAvatarUrl(),
                Name = "Command ran by: " + message.Author.Username + "#" + message.Author.Discriminator,
            };
        }

        public static Embed CreateErrorResponse(SocketMessage message, IResult result)
        {
            return new EmbedBuilder()
            {
                Color = (Color)Program.Config["responseError"],
                Description = $"{result.ErrorReason}",
                Author = GetAuthorOfMessage(message),
            }.Build();
        }
    }
}