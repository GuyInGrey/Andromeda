﻿using Discord.WebSocket;
using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace Andromeda.Games
{
    public abstract class Game
    {
        public string PartyID { get; }

        public Party InParty => Party.FindParty(PartyID);

        public abstract int MaxPlayerCount { get; }
        public abstract int MinPlayerCount { get; } 
        
        public abstract string Name { get; }

        public SocketGuildUser[] Players { get; }

        public Game(string partyID, SocketGuildUser[] players)
        {
            if (players.Length > MaxPlayerCount || players.Length < MinPlayerCount)
            {
            }
            else
            {
                var mentionString = "";

                foreach (var p in players)
                {
                    mentionString += p.Username + "#" + p.Discriminator;
                }

                PartyID = partyID;
                SendMessage("Starting Game: " + Name + "\nPlayers: " + mentionString);
                Players = players;
                Start();
            }
        }

        public abstract void Start();

        public abstract void MessageReceived(SocketMessage message, SocketGuild guild, SocketGuildUser author);

        public void SendMessage(string message) => InParty.Channel.SendMessageAsync(message).GetAwaiter().GetResult();

        public void SendImage(Bitmap image, string message)
        {
            var ID = Guid.NewGuid().ToString().Replace("-", "") + ".png";

            image.Save(ID);

            InParty.Channel.SendFileAsync(ID, message).GetAwaiter().GetResult();

            File.Delete(ID);
        }
    }
}