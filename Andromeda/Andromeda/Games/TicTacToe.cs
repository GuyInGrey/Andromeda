﻿using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace Andromeda.Games
{
    public class TicTacToe : Game
    {
        // 0: Neutral
        // 1: Player 0 / X
        // 2: Player 1 / O
        int[] Board = new int[9];

        int[][] WinningCombinations = new int[8][]
        {
            //Horizontal
            new int[] { 0, 1, 2 },
            new int[] { 3, 4, 5 },
            new int[] { 6, 7, 8 },
            //Verticle
            new int[] { 0, 3, 6 },
            new int[] { 1, 4, 7 },
            new int[] { 2, 5, 8 },
            //Diaganol
            new int[] { 0, 4, 8 },
            new int[] { 2, 4, 6},
        };

        //0 = Player 0
        //1 = Player 1
        //2 = Player 0 Victory
        //3 = Player 1 Victory
        //4 = Draw
        int Turn = -1;

        public TicTacToe(string partyID, SocketGuildUser[] players) : base(partyID, players) { }

        public override int MaxPlayerCount => 2;

        public override int MinPlayerCount => 2;
            
        public override string Name => "Tic-Tac-Toe";

        public Bitmap BuildImage()
        {
            CheckVictor();

            var size = 150;
            var SO3 = size / 3;

            var b = new Bitmap(size, size);

            using (var g = Graphics.FromImage(b))
            {
                var linePen = new Pen(Color.DarkGray, 4);
                var shapePen = new Pen(Color.Black, 4);
                var backdropBrush = Brushes.CornflowerBlue;

                g.FillRectangle(backdropBrush, 0, 0, b.Width, b.Height);
                    
                for (var y = 0; y < 3; y++)
                {
                    for (var x = 0; x < 3; x++)
                    {
                        var boardPeiceIndex = x + (y * 3);
                        var boardPeice = Board[boardPeiceIndex];

                        if (boardPeice == 1)
                        {
                            DrawX(shapePen, g, new Rectangle((x * SO3) + 7, (y * SO3) + 7, SO3 - 14, SO3 - 14));
                        }

                        if (boardPeice == 2)
                        {
                            DrawO(shapePen, g, new Rectangle((x * SO3) + 7, (y * SO3) + 7, SO3 - 14, SO3 - 14));
                        }
                    }
                }

                g.DrawLine(linePen, SO3, 0, SO3, size);
                g.DrawLine(linePen, SO3 * 2, 0, SO3 * 2, size);
                g.DrawLine(linePen, 0, SO3, size, SO3);
                g.DrawLine(linePen, 0, SO3 * 2, size, SO3 * 2);
            }

            return b;
        }

        public void CheckVictor()
        {
            //Check for Victory
            foreach (var combo in WinningCombinations)
            {
                var p0Win = true;
                var p1Win = true;

                foreach (var number in combo)
                {
                    if (Board[number] != 1)
                    {
                        p0Win = false;
                    }
                    if (Board[number] != 2)
                    {
                        p1Win = false;
                    }
                }

                if (p0Win)
                {
                    Turn = 2;
                    return;
                }
                else if (p1Win)
                {
                    Turn = 3;
                    return;
                }
            }

            var draw = true;
            //Check for Draw
            foreach (var num in Board)
            {
                if (num == 0)
                {
                    draw = false;
                }
            }

            if (draw)
            {
                Turn = 4;
                return;
            }
        }

        public void DrawX(Pen pen, Graphics g, Rectangle r)
        {
            g.DrawLine(pen, r.X, r.Y, r.X + r.Width, r.Y + r.Height);

            g.DrawLine(pen, r.X + r.Width, r.Y, r.X, r.Y + r.Height);
        }


        public void DrawO(Pen pen, Graphics g, Rectangle r)
        {
            g.DrawEllipse(pen, r);
        }

        public override void MessageReceived(SocketMessage message, SocketGuild guild, SocketGuildUser author)
        {
            var gamePrefix = Configuration.GetGamePrefix(guild.Id);
            var content = message.Content.ToLower();

            if (Turn == -1)
            {
                SendMessage("Failed to start. Restart.");
                return;
            }

            if (Turn == 2 || Turn == 3)
            {
                SendMessage("The game has ended.");
                return;
            }

            if (author.Id != Players[0].Id && author.Id != Players[1].Id) // Not one of the players of the game
            {
                return;
            }

            var selectCommand =  $"{gamePrefix}select ";

            if (content.StartsWith(selectCommand))
            {
                content = content.Substring(selectCommand.Length, content.Length - selectCommand.Length);

                if (int.TryParse(content, out var space) && space - 1 > -1 && space - 1 < 9)
                {
                    var changed = false;

                    if (author.Id == Players[0].Id)
                    {
                        if (Turn == 0)
                        {
                            if (Board[space - 1] == 0)
                            {
                                Board[space - 1] = 1;
                                changed = true;
                                Turn = 1;
                            }
                            else
                            {
                                goto spaceAlreadyFilled;
                            }
                        }
                        else
                        {
                            goto notYourTurn;
                        }
                    }
                    else if (author.Id == Players[1].Id)
                    {
                        if (Turn == 1)
                        {
                            if (Board[space - 1] == 0)
                            {
                                Board[space - 1] = 2;
                                changed = true;
                                Turn = 0;
                            }
                            else
                            {
                                goto spaceAlreadyFilled;
                            }
                        }
                        else
                        {
                            goto notYourTurn;
                        }
                    }

                    if (changed)
                    {
                        SendImage(BuildImage(), "");
                        if (Turn == 2)
                        {
                            SendMessage($"{Players[0].Mention} has won!");
                        }
                        if (Turn == 3)
                        {
                            SendMessage($"{Players[1].Mention} has won!");
                        }
                        if (Turn == 4)
                        {
                            SendMessage("DRAW!");
                        }

                        if (Turn == 0 || Turn == 1)
                        {
                            goto nowYourTurn;
                        }
                    }
                }
                else
                {
                    goto invalidBoardSpace;
                }
            }

            return;

            invalidBoardSpace:;
            SendMessage("Invalid Board Space. Pick a number, 1-9.");
            return;

            notYourTurn:;
            SendMessage($"Not your turn, {author.Mention}!");
            return;

            spaceAlreadyFilled:;
            SendMessage("That space has already been filled, " + author.Mention + "!");
            return;

            nowYourTurn:;
            SendMessage($"It is now your turn, {Players[Turn].Mention}.\n`{gamePrefix}select (1-9)`");
            return;
        }

        public override void Start()
        {
            var gamePrefix = Configuration.GetGamePrefix(InParty.Channel.GuildId);
            var randomID = 2.Random();
            Turn = randomID;
            var firstPlayer = Players[randomID];
            if (Turn == 0)
            {
                SendMessage($"***Tic-Tac-Toe***\nThe player to start is randomly determined.\n\n{Players[0].Mention} = X (STARTING)\n{Players[1].Mention} = O\n`{gamePrefix}select (1-9)`");
            }
            else if (Turn == 1)
            {
                SendMessage($"***Tic-Tac-Toe***\nThe player to start is randomly determined.\n\n{Players[0].Mention} = X\n{Players[1].Mention} = O (STARTING)\n`{gamePrefix}select (1-9)`");
            }

            SendImage(BuildImage(), "");
        }
    }
}