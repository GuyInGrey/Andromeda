﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Andromeda.Logging;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

namespace Andromeda
{
    class Program
    {
        public static Dictionary<string, object> Config { get; private set; }

        static void Main(string[] args) => new Program().RunBotAsync().GetAwaiter().GetResult();

        public static DiscordSocketClient Client;
        public static CommandService Commands;
        public static IServiceProvider Services;

        List<Thread> CommandThreads { get; set; } = new List<Thread>();

        public async Task RunBotAsync()
        {
            Configuration.Load();

            Config = new Dictionary<string, object>
            {
                ["responseNormal"] = new Color(114, 137, 218),
                ["responseError"] = new Color(255, 0, 0),

                ["tokenFile"] = Directory.GetCurrentDirectory() + "/config.token",
            };

            if (File.Exists((string)Config["tokenFile"]))
            {
                Config["token"] = File.ReadAllText((string)Config["tokenFile"]);
            }
            else
            {
                Console.WriteLine("Token file does not exist. Failed to start. Press any key to close...");
                Console.ReadKey();
                return;
            }

            Client = new DiscordSocketClient(new DiscordSocketConfig() { MessageCacheSize = 500, });

            Commands = new CommandService();

            Services = new ServiceCollection()
                .AddSingleton(Client)
                .AddSingleton(Commands)
                .BuildServiceProvider();

            //event subscriptions
            Client.Log += Log;

            await RegisterCommandsAsync();

            await Client.LoginAsync(TokenType.Bot, (string)Config["token"]);

            await Client.StartAsync();

            await Task.Delay(-1);
        }

        public async Task RegisterCommandsAsync()
        {
            Client.MessageReceived += HandleCommandAsync;

            await Commands.AddModulesAsync(Assembly.GetEntryAssembly(), Services);
        }

        private Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;

            if (message is null || message.Author.IsBot)
            {
                return Task.CompletedTask;
            }

            var argPos = 0;

            var prefix = Configuration.GetPrefix((arg.Author as SocketGuildUser).Guild.Id);
            var gamePrefix = Configuration.GetGamePrefix((arg.Author as SocketGuildUser).Guild.Id);

            if (message.HasStringPrefix(prefix, ref argPos) || message.HasStringPrefix(Client.CurrentUser.Id.ToString() + "-prefix-", ref argPos))
            {
                var context = new SocketCommandContext(Client, message);

                var thread = new Thread(new ParameterizedThreadStart(RunCommand));
                CommandThreads.Add(thread);
                thread.Start(new object[]
                    {
                        context,
                        argPos,
                        Services,
                        arg,
                        thread,
                    }
                );
            }
            else if (message.HasStringPrefix(gamePrefix, ref argPos))
            {
                Party.StaticMessageReceived(message);
            }

            return Task.CompletedTask;
        }

        public async void RunCommand(object args)
        {
            var argsArray = (object[])args;

            var context = (SocketCommandContext)argsArray[0];
            var argPos = (int)argsArray[1];
            var services = (IServiceProvider)argsArray[2];
            var message = (SocketMessage)argsArray[3];
            var thread = (Thread)argsArray[4];

            var result = await Commands.ExecuteAsync(context, argPos, Services);

            if (!result.IsSuccess)
            {
                await message.Channel.SendMessageAsync("", false, AndromedaCommand.CreateErrorResponse(message, result));

                Logger.Log($"Command Error: {result.Error}\n  -{result.ErrorReason}\n    -\"{message}\" - {message.Author.Username}#{message.Author.Discriminator}", LogMessageCategory.Warning);
            }

            CommandThreads.Remove(thread);
        }

        private Task Log(LogMessage arg)
        {
            Logger.Log(arg.Message, LogMessageCategory.Convert(arg.Severity));

            return Task.CompletedTask;
        }
    }
}