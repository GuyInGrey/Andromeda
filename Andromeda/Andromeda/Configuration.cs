﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Andromeda
{
    public static class Configuration
    {
        public static Dictionary<string, object> DefaultConfig = new Dictionary<string, object>
        {
            ["prefix"] = "+",
            ["gamePrefix"] = "[",
        };

        private static string SaveFolder { get; } = Directory.GetCurrentDirectory() + @"\AndromedaServerConfigs\";

        public static string Extension { get; } = @"serverconfig";

        public static string GetPrefix(ulong serverID)
        {
            return GetValue(serverID, "prefix") == null ? (string)DefaultConfig["prefix"] : GetValue(serverID, "prefix").ToString();
        }

        public static string GetGamePrefix(ulong serverID)
        {
            return GetValue(serverID, "gamePrefix") == null ? (string)DefaultConfig["gamePrefix"] : GetValue(serverID, "gamePrefix").ToString();
        }

        private static Dictionary<ulong, Dictionary<string, object>> ServerConfigurations { get; } = new Dictionary<ulong, Dictionary<string, object>>();

        public static object GetValue(ulong server, string key)
        {
            if (!ServerConfigurations.ContainsKey(server))
            {
                return null;
            }

            if (!ServerConfigurations[server].ContainsKey(key))
            {
                return null;
            }

            return ServerConfigurations[server][key];
        }

        public static void SetValue(ulong server, string key, object value)
        {
            if (!ServerConfigurations.ContainsKey(server))
            {
                return;
            }

            ServerConfigurations[server][key] = value;
            Save(server);
        }

        public static bool ContainsServerConfig(ulong id)
        {
            return ServerConfigurations.ContainsKey(id);
        }

        private static void Save(ulong server)
        {
            if (!ServerConfigurations.ContainsKey(server))
            {
                return;
            }

            var dictionary = ServerConfigurations[server];

            var json = JsonConvert.SerializeObject(dictionary);

            if (!Directory.Exists(SaveFolder))
            {
                Directory.CreateDirectory(SaveFolder);
            }

            var path = $"{SaveFolder}{server}.{Extension}";

            if (File.Exists(path))
            {
                File.Delete(path);
            }

            using (var fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                using (var sw = new StreamWriter(fs))
                {
                    sw.Write(json);
                    sw.Flush();
                }
            }
        }

        public static void Load()
        {
            if (!Directory.Exists(SaveFolder))
            {
                return;
            }

            foreach (var file in Directory.GetFiles(SaveFolder))
            {
                if (Path.GetExtension(file) != "." + Extension)
                {
                    continue;
                }


                if (ulong.TryParse(Path.GetFileNameWithoutExtension(file), out var server))
                {
                    using (var fs = new FileStream(file, FileMode.Open))
                    {
                        using (var sr = new StreamReader(fs))
                        {
                            var json = sr.ReadToEnd();
                            var config = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                            ServerConfigurations[server] = config;
                        }
                    }
                }
            }
        }

        public static bool ResetServer(ulong server)
        {
            if (ServerConfigurations.ContainsKey(server))
            {
                ServerConfigurations.Remove(server);
            }

            ServerConfigurations[server] = new Dictionary<string, object>(DefaultConfig);
            Save(server);

            return true;
        }

        public static ConfigOption[] ListOptions(ulong serverID)
        {
            if (!ServerConfigurations.ContainsKey(serverID))
            {
                return null;
            }

            var options = new List<ConfigOption>();

            foreach (var diction in ServerConfigurations[serverID])
            {
                options.Add(new ConfigOption(diction.Key, diction.Value));
            }

            return options.ToArray();
        }
    }

    public class ConfigOption
    {
        public string Key { get; set; }
        public object Value { get; set; }

        public ConfigOption(string key, object val)
        {
            Key = key;
            Value = val;
        }
    }
}