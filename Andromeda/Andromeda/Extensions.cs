﻿using System;
using System.Collections.Generic;
using System.Text;
using Discord.WebSocket;

namespace Andromeda
{
    public static class Extensions
    {
        public static Random r = new Random();

        public static int Random(this int a)
        {
            return r.Next(0, a);
        }

        public static Party Party(this SocketUser user)
        {
            return Andromeda.Party.FindParty(user);
        }
    }
}