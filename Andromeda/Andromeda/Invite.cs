﻿using System.Collections.Generic;
using Discord.WebSocket;

namespace Andromeda
{
    public class Invite
    {
        public Party InvitingParty { get; set; }
        public SocketGuildUser Invitee { get; set; }

        public int ID => Invites.IndexOf(this);

        public Invite(Party party, SocketGuildUser invitee)
        {
            Invitee = invitee;
            InvitingParty = party;

            Invites.Add(this);
        }

        public void Accept()
        {
            InvitingParty.Add(Invitee);
            Invites.Remove(this);
        }

        public void Cancel()
        {
            Invites.Remove(this);
        }

        public static List<Invite> Invites { get; set; } = new List<Invite>();
    }
}