﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Rest;
using Discord.WebSocket;

namespace Andromeda
{
    public class Party
    {
        public static void StaticMessageReceived(SocketMessage message)
        {
            foreach (var p in Parties)
            {
                if (p.Channel.Id == message.Channel.Id)
                {
                    p.MessageReceived(message);
                }
            }
        }

        public void MessageReceived(SocketMessage message)
        {
            Game?.MessageReceived(message, (message.Channel as SocketGuildChannel).Guild, message.Author as SocketGuildUser);
        }

        public string GUID { get; }

        public SocketGuildUser Leader { get; set; }

        public List<SocketGuildUser> Players { get; set; } = new List<SocketGuildUser>();

        public RestTextChannel Channel { get; set; }

        private OverwritePermissions EveryonePerms { get; }
        private OverwritePermissions MemberPerms { get; }

        public Games.Game Game { get; set; }

        public void Add(SocketGuildUser invitee)
        {
            Players.Add(invitee);
            Channel.AddPermissionOverwriteAsync(invitee, MemberPerms);
            Channel.SendMessageAsync($"{invitee.Mention} has joined the party");
        }

        public Party(SocketGuildUser leader)
        {
            MemberPerms = new OverwritePermissions(PermValue.Deny, PermValue.Deny, PermValue.Allow, PermValue.Allow, PermValue.Allow);

            EveryonePerms = new OverwritePermissions(PermValue.Deny, PermValue.Deny, PermValue.Allow, PermValue.Deny, PermValue.Deny);

            Leader = leader;
            Players.Add(leader);

            do
            {
                GUID = Guid.NewGuid().ToString().Replace("-", "");
            } while (_Parties.ContainsKey(GUID));

            _Parties.Add(GUID, this);
        }

        public async Task CreateChannels(SocketGuildUser leader)
        {
            var channel = await leader.Guild.CreateTextChannelAsync(GUID, x => {
                x.Name = GUID;
                x.IsNsfw = false;
                x.Topic = $"This is the channel for the party owned by {leader.Mention}";
                x.CategoryId = ulong.Parse(Configuration.GetValue(leader.Guild.Id, "gameCategoryID").ToString());
                });
            Channel = channel;
            await channel.AddPermissionOverwriteAsync(leader.Guild.EveryoneRole, EveryonePerms);
            await channel.AddPermissionOverwriteAsync(leader, MemberPerms);
            
            await channel.SendMessageAsync($"{leader.Mention}, here's your new party channel!");
        }

        public void CreateInvite(SocketGuildUser invitee)
        {
            Invite.Invites.Add(new Invite(this, invitee));
        }

        private static Dictionary<string, Party> _Parties { get; set; } = new Dictionary<string, Party>();

        public static List<Party> Parties => _Parties.Values.ToList();

        public static Party FindParty(SocketUser user)
        {
            Party party = null;

            foreach (var p in Party.Parties)
            {
                if (p.Players.FirstOrDefault(x => x.Id == user.Id) != null)
                {
                    party = p;
                }
            }

            return party;
        }

        public static Party FindParty(string guid)
        {
            foreach (var p in Party.Parties)
            {
                if (p.GUID == guid)
                {
                    return p;
                }
            }

            return null;
        }

        public async Task Delete()
        {
            _Parties.Remove(GUID);
            await Channel.DeleteAsync();
            Players.Clear();
            Leader = null;
        }

        public void Kick(SocketGuildUser socketGuildUser)
        {
            Channel.RemovePermissionOverwriteAsync(socketGuildUser);
            Players.Remove(socketGuildUser);
            Channel.SendMessageAsync($"{socketGuildUser.Mention} has left the party");
        }
    }
}